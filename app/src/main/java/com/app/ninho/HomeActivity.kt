package com.app.ninho

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.app.ninho.controllers.HomeListController
import com.app.ninho.controllers.ProductListController
import com.app.ninho.model.User
import com.app.ninho.utils.GlideApp
import com.app.ninho.utils.Utils
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.navigation.NavigationView

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var homeListController: HomeListController
    private lateinit var headerName : TextView
    private lateinit var headerEmail : TextView
    private lateinit var headerAvatar : ImageView
    private lateinit var user : User
    private lateinit var mIntent : Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = ""
        setSupportActionBar(toolbar)


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val header = navView.getHeaderView(0)
        headerName = header.findViewById(R.id.tvName)
        headerEmail = header.findViewById(R.id.tvEmail)
        headerAvatar = header.findViewById(R.id.imgvAvatar)

//        getUser()

        navView.setNavigationItemSelectedListener(this)

//        homeListController = ProductListController()
        supportFragmentManager.beginTransaction().replace(R.id.container, ProductListController()).commit()
    }


    private fun buildHeader() {
        headerName.text = user.firstName
        Utils.debug(user.email)
        headerEmail.text = user.email
        GlideApp.with(this).load(user.avatar).diskCacheStrategy(
            DiskCacheStrategy.RESOURCE).fitCenter().apply(RequestOptions.circleCropTransform()).into(headerAvatar)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (supportFragmentManager.backStackEntryCount > 1){
                supportFragmentManager.popBackStack()}
            else super.onBackPressed()
        }
    }

    /*override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }*/

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        var b = false
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
            }
            R.id.nav_products -> {
//                mIntent = Intent(this@HomeActivity, EditProfileController::class.java)
//                mIntent.putExtra("USER", user)
//                b = true
            }
        }

        if(b) {
            startActivity(mIntent)
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }


}
