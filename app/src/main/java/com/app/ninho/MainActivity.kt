package com.app.ninho

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
FELIPE
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}