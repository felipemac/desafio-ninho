package com.app.ninho.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.ninho.R
import com.app.ninho.model.ResponseVitrine
import com.app.ninho.viewholder.VitrinesViewHolder

/**
FELIPE
 */
class VitrinesAdapter(val list: MutableList<ResponseVitrine>) : RecyclerView.Adapter<VitrinesViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VitrinesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val layout = R.layout.item_vitrine
        val view = inflater.inflate(layout, parent, false)

        return VitrinesViewHolder(view)
    }

    override fun onBindViewHolder(holder: VitrinesViewHolder, position: Int) {
        holder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

/**
class ClassificationTableAdapter(val classification: List<ClassificationTeam>, val type: Int) :
RecyclerView.Adapter<ClassificationTableViewHolder>() {

override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClassificationTableViewHolder {
val context = parent.context
val inflater = LayoutInflater.from(context)

var layout = R.layout.item_header_team_classification_table
if (type == Constants.CLASSIFICATION_TABLE.TYPE.STATS) {
layout = R.layout.item_stats_team_classification_table
}
val view = inflater.inflate(layout, parent, false)


return ClassificationTableViewHolder(view, context)
}


override fun onBindViewHolder(holder: ClassificationTableViewHolder, position: Int) {
val team = classification[position]

holder.bindData(team, type, position)


}

override fun getItemCount(): Int {
return classification.count()
}


}*/