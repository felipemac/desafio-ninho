package com.app.ninho.api


import com.app.ninho.model.ResponseList
import com.app.ninho.model.ResponseProductDetails
import com.app.ninho.model.ResponseVitrine
import retrofit2.Call
import retrofit2.http.GET

interface ConnectionService {

    /***
     * Chamada para listar as listas de compra ativas na home
     */
    @GET(".")
    fun list() : Call<ResponseList>

    @GET(".")
    fun productDetails() : Call<ResponseProductDetails>

    @GET(".")
    fun vitrine() : Call<List<ResponseVitrine>>
    /***
     * Chamada para retornar o campeonato vigente
     *//*
    @GET(".")
    fun getChampionship(): Call<ResponseDefault<ResponseChampionship>>

    */
    /***
     * Chamada para Alistar os jogos da agenda (que aparece na home)
     *//*
    @GET("{championship}")
    fun getHomeMatches(@Path("championship") championship: String): Call<ResponseDefault<HomeMatches>>

    */
    /***
     * Chamada para API de listar os artilheiros da home
     *//*
    @GET("{championship}")
    fun getHomeTopScorer(@Path("championship") championship: String): Call<ResponseDefault<List<Scorer>>>

    */
    /***
     * Chamada para API de listar os artilheiros da home
     *//*
    @GET("{championship}")
    fun getHomeClassification(@Path("championship") championship: String): Call<ResponseDefault<List<ClassificationTeam>>>



    */
    /***
     * Chamada para API de listar a tabela de classificação
     *//*
    @GET("{championship}")
    fun getClassificationTable(@Path("championship") championship: String): Call<ResponseDefault<List<ClassificationTeam>>>

    */
    /***
     * Chamada para API de listar a tabela de classificação
     *//*
    @GET("{championship}")
    fun getScorers(@Path("championship") championship: String): Call<ResponseDefault<List<Scorer>>>

    */
    /***
     * Chamada para API de listar os jogos da rodada
     *//*
    @GET("{championship}/{round}")
    fun getRoundMatches(@Path("championship") championship: String,
                        @Path("round") rounds: String): Call<ResponseDefault<List<Match>>>


    */
    /***
     * Chamada para API de listar a tabela de cartões
     *//*
    @GET("{championship}")
    fun getTotalCards(@Path("championship") championship: String): Call<ResponseDefault<List<PlayerCards>>>

    */
    /***
     * Chamada para API de listar os lances do jogo ao vivo (quando entra na tela ou força a atualização com swipe)
     *//*
    @GET("{championship}")
    fun getMoveOfMatch(@Path("championship") championship: String): Call<ResponseDefault<ResponseLiveMatch>>

    */
    /***
     * Chamada para API de listar os lances do jogo ao vivo (a cada intervalo)
     *//*
    @FormUrlEncoded
    @POST("{championship}")
    fun getMoveOfMatchInterval(@Path("championship") championship: String, @Field("ultimaLeitura") param : String): Call<ResponseDefault<ResponseLiveMatch>>

    */
    /***
     * Chamada para API de listar os dados do time (nome, jogadores...)
     *//*
    @GET("{team}/{championship}")
    fun getTeam(@Path("championship") championship: String, @Path("team") team: String): Call<ResponseDefault<ResponseTeam>>*/
}