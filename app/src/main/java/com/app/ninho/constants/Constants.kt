package com.app.ninho.constants

class Constants {

    object END_POINTS {
        val SERVER = "http://www.mocky.io/v2/"
        val ACCOUNT = SERVER + "5d1b4fd23400004c000006e1/"
        val SHOPPING_LIST = SERVER + "5d1b4f0f34000074000006dd/"
        val VITRINE = SERVER + "5d1b507634000054000006ed/"
//        val REGISTER = ACCOUNT + "register"
/*        val ARENA_HOME_TOP_SCORER = ARENA_HOME + "tabela-artilharia/"
        val ARENA_HOME_CLASSIFICATION = ARENA_HOME + "tabela-campeonato/"
        val ARENA_CHAMPIONSHIP = ARENA_SERVER + "campeonato/"
        val ARENA_CLASSIFICATION = ARENA_CHAMPIONSHIP + "classificacao/"
        val ARENA_ROUNDS_MATCHES = ARENA_CHAMPIONSHIP + "jogos/"
        val ARENA_CARDS = ARENA_CHAMPIONSHIP + "cartoes/"
        val ARENA_SCORERS = ARENA_CHAMPIONSHIP + "artilharia/"
        val ARENA_REAL_TIME_MATCH = ARENA_CHAMPIONSHIP + "ao-vivo/"
        val ARENA_TEAM = ARENA_SERVER + "time/dados/"*/
    }

/*    object LINKS {
        private val BASE_LINK = "https://arena.futfanatics.com.br/"
        val CHAMPIONSHIP_REGULATION = "${BASE_LINK}regulamento.html"
        val CHAMPIONSHIP_HISTORY = "${BASE_LINK}historia.html"
    }*/

    object SHARED_PREFERENCES {
        val FILE = "GENERAL"
        val AUTH = "AUTH"
        val EXPIRE = "EXPIRE"
        val CHAMP_NAME = "CHAMPIONSHIP_NAME"
        val ROUNDS = "ROUNDS"
    }

    object MESSAGES {
        val BLANK_FIELDS = "Todos os campos devem ser preenchidos"
        val IMAGE_FAILURE = "Ocorreu um problema ao carregar a imagem"
    }
}
