package com.app.ninho.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.ninho.BuildConfig
import com.app.ninho.R
import com.app.ninho.adapter.HomeListAdapter
import com.app.ninho.model.MList
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.home_view.*

/**
FELIPE
 */
class HomeListController : Fragment() {

    private var list = mutableListOf<MList>()
    private lateinit var adapter: HomeListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.home_view, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        versionName.text = "(${BuildConfig.VERSION_NAME})"
        versionCode.text = "(${BuildConfig.VERSION_CODE})"

//        recyclerView.setHasFixedSize(true)
//        recyclerView.layoutManager = GridLayoutManager(context, 2)

//        getLists()
    }

    /*private fun getLists() {

        val auth = DataPreferences(context!!).getStoredString(AUTH)
        val call = RetrofitInitializer.createRetrofitInstanceDefault(Constants.END_POINTS.SHOPPING_LIST)
            .list(auth!!)

        call.enqueue(object : Callback<ResponseDefault<List<MList>>> {
            override fun onResponse(
                call: Call<ResponseDefault<List<MList>>>,
                response: Response<ResponseDefault<List<MList>>>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    val data = response.body()?.data
                    list = data as MutableList<MList>
                }
                else{
                    DialogUtils.showErrorDialog(context, response.code(), response.errorBody()?.string())
                }
                buildData()
            }

            override fun onFailure(call: Call<ResponseDefault<List<MList>>>, t: Throwable) {
                t.printStackTrace()
                DialogUtils.showErrorActionDialog(context)
            }
        })
    }*/

   /* private fun buildData() {
        if (list.size > 0) {
            tvNoList.visibility = GONE
        }
        adapter = HomeListAdapter(list)
        recyclerView.adapter = adapter
    }

    fun updateList(newList: MList?) {
        try {
            tvNoList.visibility = GONE
            list.add(0, newList!!)
            adapter.notifyItemInserted(0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }*/

}