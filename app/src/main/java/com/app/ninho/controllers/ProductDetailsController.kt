package com.app.ninho.controllers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.ninho.R
import com.app.ninho.adapter.MoreInfosAdapter
import com.app.ninho.adapter.VitrinesAdapter
import com.app.ninho.api.RetrofitInitializer
import com.app.ninho.constants.Constants
import com.app.ninho.model.MaisInformacoes
import com.app.ninho.model.ResponseProductDetails
import com.app.ninho.model.ResponseVitrine
import kotlinx.android.synthetic.main.more_infos.*
import kotlinx.android.synthetic.main.product_details.*
import kotlinx.android.synthetic.main.product_details.view.*
import kotlinx.android.synthetic.main.product_details_base.*
import kotlinx.android.synthetic.main.product_details_base.tvDescription
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
FELIPE
 */
class ProductDetailsController : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_details)

        getProductDetails()

        getVitrine()
    }


    private fun getProductDetails() {
        val call = RetrofitInitializer.createRetrofitInstanceDefault(Constants.END_POINTS.ACCOUNT)
            .productDetails()

        call.enqueue(object : Callback<ResponseProductDetails> {
            override fun onResponse(
                call: Call<ResponseProductDetails>,
                response: Response<ResponseProductDetails>
            ) {
                if (response.isSuccessful) {
                    buildData(response.body())
                }
            }

            override fun onFailure(call: Call<ResponseProductDetails>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }


    private fun getVitrine() {
        val call = RetrofitInitializer.createRetrofitInstanceDefault(Constants.END_POINTS.VITRINE)
            .vitrine()

        call.enqueue(object : Callback<List<ResponseVitrine>>{
            override fun onResponse(
                call: Call<List<ResponseVitrine>>,
                response: Response<List<ResponseVitrine>>
            ) {
                if(response.isSuccessful) {
                    buildVitrine(response.body())
                }
            }

            override fun onFailure(call: Call<List<ResponseVitrine>>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    private fun buildVitrine(body: List<ResponseVitrine>?) {

        container.tvDescription.text = "QUEM VIU COMPROU"
        container.rvValues.setHasFixedSize(true)
        container.rvValues.adapter = VitrinesAdapter(body as MutableList<ResponseVitrine>)
        container.rvValues.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

    }

    private fun buildData(body: ResponseProductDetails?) {
        tvName.text = body?.nome
        tvDescription.text = body?.descricao

        tvOldPrice.text = "R$ ${body?.modelo!!.padrao.preco.precoAnterior} "
        tvPrice.text = "R$ ${body.modelo.padrao.preco.precoAtual} "
        tvInstallment.text = body.modelo.padrao.preco.planoPagamento

        buildMoreInfo(body.maisInformacoes)

    }

    private fun buildMoreInfo(moreInfos: List<MaisInformacoes>) {

        rvMoreInfos.setHasFixedSize(true)
        rvMoreInfos.adapter = MoreInfosAdapter(moreInfos as MutableList<MaisInformacoes>)
        rvMoreInfos.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvMoreInfos.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

    }

}