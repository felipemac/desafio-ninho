package com.app.ninho.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.app.ninho.adapter.HomeListAdapter
import com.app.ninho.api.RetrofitInitializer
import com.app.ninho.constants.Constants
import com.app.ninho.model.ProdutosItem
import com.app.ninho.model.ResponseList
import com.app.ninho.utils.DialogUtils
import kotlinx.android.synthetic.main.content_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.recyclerview.widget.DividerItemDecoration
import com.app.ninho.R


/**
FELIPE
 */
class ProductListController : Fragment() {

    private var list = mutableListOf<ProdutosItem>()
    private lateinit var adapter: HomeListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.list_product_view, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                context!!,
                DividerItemDecoration.VERTICAL
            )
        )

        recyclerView.addItemDecoration(
            DividerItemDecoration(
                context!!,
                DividerItemDecoration.HORIZONTAL
            )
        )

        getLists()
    }

    private fun getLists() {

        val call = RetrofitInitializer.createRetrofitInstanceDefault(Constants.END_POINTS.SHOPPING_LIST)
            .list()

        call.enqueue(object : Callback<ResponseList> {
            override fun onResponse(
                call: Call<ResponseList>,
                response: Response<ResponseList>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    val data = response.body()?.produtos
                    list = data as MutableList<ProdutosItem>
                }
                else{
//                    DialogUtils.showErrorDialog(context, response.code(), response.errorBody()?.string())
                }
                buildData()
            }

            override fun onFailure(call: Call<ResponseList>, t: Throwable) {
                t.printStackTrace()
                DialogUtils.showErrorActionDialog(context)
            }
        })
    }

    private fun buildData() {
        if (list.size > 0) {
//            tvNoList.visibility = GONE
        }
        adapter = HomeListAdapter(list)

        recyclerView.adapter = adapter
    }

//    fun updateList(newList: MList?) {
//        try {
//            tvNoList.visibility = GONE
//            list.add(0, newList!!)
//            adapter.notifyItemInserted(0)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }

}