package com.app.ninho.model

import com.google.gson.annotations.SerializedName

data class Preco(@SerializedName("precoAtual")
                 val precoAtual: Int = 0,
                 @SerializedName("valorParcela")
                 val valorParcela: Double = 0.0,
                 /*@SerializedName("descontoFormaPagamento")
                 val descontoFormaPagamento: Double? = null,*/
                 @SerializedName("porcentagemDesconto")
                 val porcentagemDesconto: Int = 0,
                 @SerializedName("planoPagamento")
                 val planoPagamento: String = "",
                 @SerializedName("quantidadeMaximaParcelas")
                 val quantidadeMaximaParcelas: Int = 0,
                 @SerializedName("precoAnterior")
                 val precoAnterior: Double = 0.0)