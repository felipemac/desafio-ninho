package com.app.ninho.model

import com.google.gson.annotations.SerializedName

data class ProdutosItem(@SerializedName("preco")
                        val preco: Preco,
                        @SerializedName("disponivel")
                        val disponivel: Boolean = false,
                        @SerializedName("nome")
                        val nome: String = "",
                        @SerializedName("classificacao")
                        val classificacao: Int = 0,
                        @SerializedName("id")
                        val id: Int = 0,
                        @SerializedName("sku")
                        val sku: Int = 0,
                        @SerializedName("imagemUrl")
                        val imagemUrl: String = "",
                        @SerializedName("descricao")
                        val descricao: String = "")