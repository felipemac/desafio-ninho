package com.app.ninho.model

data class ResponseDefault<T>(
    var isStatus: Boolean = false,
    var httpCode: Int = 0,
    var error_msg: String = "",
    var data: T? = null
)