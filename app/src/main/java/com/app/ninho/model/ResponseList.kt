package com.app.ninho.model

import com.google.gson.annotations.SerializedName

data class ResponseList(@SerializedName("produtos")
                        val produtos: List<ProdutosItem>?,
                        @SerializedName("quantidade")
                        val quantidade: Int = 0)