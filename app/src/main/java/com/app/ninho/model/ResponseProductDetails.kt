package com.app.ninho.model


data class ResponseProductDetails (

	val id : Int,
	val nome : String,
	val descricao : String,
	val retiraEmLoja : Boolean,
	val categorias : List<Categorias>,
	val maisInformacoes : List<MaisInformacoes>,
	val marca : Marca,
	val modelo : Modelo,
	val urlVideo : String
)