package com.app.ninho.model

/**
FELIPE
 */
data class ResponseVitrine (

    val id : Int,
    val sku : Int,
    val nome : String,
    val imagemUrl : String,
    val precoAtual : Double,
    val precoAnterior : Double,
    val percentualCompra : Int,
    val classificacao : Float,
    val parcelamento : String
)