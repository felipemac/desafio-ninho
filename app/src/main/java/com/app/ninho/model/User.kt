package com.app.ninho.model

import java.io.Serializable

/**
FELIPE
 */
data class User(var firstName:String, var lastName:String,
                var email:String, var avatar:String, var facebookId:String) : Serializable