package com.app.ninho.utils

import android.content.Context
import android.content.SharedPreferences
import com.app.ninho.constants.Constants
import com.app.ninho.constants.Constants.SHARED_PREFERENCES.AUTH
import com.app.ninho.constants.Constants.SHARED_PREFERENCES.EXPIRE

class DataPreferences(context: Context) {

    private val mSharedPreferences: SharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES.FILE, Context.MODE_PRIVATE)

    fun storeString(key: String, value: String) {
        this.mSharedPreferences.edit().putString(key, value).apply()
    }

    fun getStoredString(key: String): String? {
        return this.mSharedPreferences.getString(key, null)
    }

    fun storeBoolean(key: String, value: Boolean) {
        this.mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    fun getStoredBoolean(key: String): Boolean {
        return this.mSharedPreferences.getBoolean(key, false)
    }

    fun removeStoredString (key: String) {
        this.mSharedPreferences.edit().remove(key).apply()
    }

    fun clearUserData() {
        removeStoredString(AUTH)
        removeStoredString(EXPIRE)
    }
}