package com.app.ninho.utils;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class DialogUtils {
    private static ProgressDialog progressDialog;

    public static void showProgressDialog(Context context, String title, String message) {
        progressDialog = ProgressDialog.show(context, title, message, true, false);
    }

    public static void showProgressDialog(Context context) {
        showProgressDialog(context, "", "");
    }

    public static void dismissProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void showErrorDialog(Context context, int errorCode, String str) {
        String title = "";//"Erro "+errorCode;
        String message = "";

        try {
            byte[] b = str.getBytes(StandardCharsets.UTF_8);
            String json = new String(b);
            JSONObject jsonObject = new JSONObject(json);

            if (jsonObject.has("errorMsg")) {
                message = jsonObject.getString("errorMsg");
            } else if (jsonObject.has("error_description")) {
                message = jsonObject.getString("error_description");
            } else {
                title = "";//"Erro " + errorCode;
                message = str;
            }
        } catch (Exception e) {
            e.printStackTrace();
            title = "";//"Erro " + errorCode;
            message = str;
        }

        try {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .setCancelable(false)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showErrorDialogFriendly(Context context, int errorCode, String msg) {
        String title;
        String message = "";

        switch (errorCode) {
            case 400:
                title = "Opps..";
                message = "Tivemos um problema técnico parece que chutaram a bola para fora do gramado, mas nossos gandulas já foram buscar.";
                break;
            case 401:
                title = "Não Autorizado";
                message = "Subiu a bandeira, rolou um impedimento aqui, você precisa estar logado para acessar essa função";
                break;
            case 403:
                title = "Acesso Negado";
                message = "Subiu a bandeira, rolou um impedimento aqui, você não tem autorização para acessar essa função.";
                break;
            case 404:
                title = "Opps..";
                message = "A súmula sumiu, não conseguimos recuperar os dados.";
                break;
            case 500:
                title = "Mau Tempo";
                message = "Campo alagou e o jogo foi cancelado, se persistir o problema entre em contato com nossa comissão técnica.";
                break;
            default:
                title = "Opps..";
                message = "Algum problema técnico no jogo, estamos trabalhando para resolver.";
        }

        try {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .setCancelable(false)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showConnectionErrorDialog(Context context) {
        String message = "";

//        if (Utils.isNetworkAvailable(context))
//        {
//            message = "Não foi possível conectar ao servidor";
//        }
//        else
//        {
//            message = "Por favor, verifique a conexão com a internet e tente novamente";
//        }
        try {

            new AlertDialog.Builder(context)
                    .setTitle("Erro de conexão")
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showMessageDialog(Context context, String title, String message) {
        try {
            new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showErrorActionDialog(Context context) {
        showMessageDialog(context, "Opps..", "Algum problema técnico no jogo, estamos trabalhando para resolver.");
    }

    public static void showErrorActionDialog(Context context, String message) {
        showMessageDialog(context, "Erro", message);
    }
}

