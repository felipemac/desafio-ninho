package com.app.ninho.utils


import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
FELIPE
 */
//classe necessária para o funcionamento do GLIDE, spós determinada versão precisamos do GlideApp, essa classe configura isso
@GlideModule
class MyAppGlideModule : AppGlideModule()