package com.app.ninho.utils

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.reflect.TypeToken

class Utils {

    //    companion object {
//        fun listToJson(t:List<*>) : String {
//            val gson = Gson()
//            val type = TypeToken()
//        }
//    }
    companion object {
        inline fun <reified T> genericType() = object : TypeToken<T>() {}.type

        fun<T> loadImage(url:T, target: ImageView, context:Context) {
            GlideApp.with(context).load(url).diskCacheStrategy(
                DiskCacheStrategy.RESOURCE).fitCenter().into(target)
        }

        fun toast(toastMessage : String, context : Context) {
            try{
                Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show()
            }catch (e:Exception){
                e.printStackTrace()
            }
        }

        fun debug(debugMessage : String) {
            println("[DEBUG] $debugMessage")
        }


    }

}