package com.app.ninho.viewholder

import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.ninho.adapter.MoreInfosChildAdapter
import com.app.ninho.model.MaisInformacoes
import com.app.ninho.model.Valores
import kotlinx.android.synthetic.main.item_more_infos.view.*

/**
FELIPE
 */
class MoreInfosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindData(mList: MaisInformacoes) {
        /*val listName : TextView = itemView.findViewById(R.id.tvListName)
        val total : TextView = itemView.findViewById(R.id.tvTotal)*/
        /*itemView.tvProductName.text = mList.nome
        itemView.tvOldPrice.text = mList.preco.precoAnterior.toString()
        itemView.tvPrice.text = mList.preco.precoAtual.toString()

        Utils.loadImage(mList.imagemUrl, itemView.imageView, itemView.context)

        itemView.setOnClickListener {
            itemView.context.startActivity(Intent(itemView.context, ProductDetailsController::class.java))
        }*/
        itemView.tvDescription.text = mList.descricao
        itemView.rvValues.setHasFixedSize(true)
        itemView.rvValues.adapter = MoreInfosChildAdapter(mList.valores as MutableList<Valores>)
        itemView.rvValues.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        itemView.rvValues.addItemDecoration(
            DividerItemDecoration(
                itemView.context!!,
                DividerItemDecoration.VERTICAL
            )
        )

    }
}

/***
class ClassificationTableViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {

fun bindData(team: ClassificationTeam, type: Int, pos: Int) {
if (type == Constants.CLASSIFICATION_TABLE.TYPE.HEADER) {
val position: TextView = itemView.findViewById(R.id.tvPosition)
val teamName: TextView = itemView.findViewById(R.id.teamName)
val imgTeam: ImageView = itemView.findViewById(R.id.imgTeam)

position.text = (pos+1).toString() //array começa index ZERO, logo, devemos corrigir pra ser a partir do UM
teamName.text = team.name
//            GlideApp.with(context).load(team.shield).fitCenter().into(imgTeam)
Utils.loadImage(team.shield, imgTeam, context, R.drawable.team_placeholder)

configureTransition(team.id)
} else {
bindDataStats(team)
}
}*/