package com.app.ninho.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.app.ninho.model.ResponseVitrine
import com.app.ninho.utils.Utils
import kotlinx.android.synthetic.main.item_product_list.view.*

/**
FELIPE
 */
class VitrinesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindData(mList: ResponseVitrine) {
        /*val listName : TextView = itemView.findViewById(R.id.tvListName)
        val total : TextView = itemView.findViewById(R.id.tvTotal)*/
        itemView.tvProductName.text = mList.nome
        itemView.tvOldPrice.text = mList.precoAnterior.toString()
        itemView.tvPrice.text = mList.precoAtual.toString()

        Utils.loadImage(mList.imagemUrl, itemView.imageView, itemView.context)
//
//        itemView.setOnClickListener {
//            itemView.context.startActivity(Intent(itemView.context, ProductDetailsController::class.java))
//        }

    }
}

/***
class ClassificationTableViewHolder(itemView: View, val context: Context) : RecyclerView.ViewHolder(itemView) {

fun bindData(team: ClassificationTeam, type: Int, pos: Int) {
if (type == Constants.CLASSIFICATION_TABLE.TYPE.HEADER) {
val position: TextView = itemView.findViewById(R.id.tvPosition)
val teamName: TextView = itemView.findViewById(R.id.teamName)
val imgTeam: ImageView = itemView.findViewById(R.id.imgTeam)

position.text = (pos+1).toString() //array começa index ZERO, logo, devemos corrigir pra ser a partir do UM
teamName.text = team.name
//            GlideApp.with(context).load(team.shield).fitCenter().into(imgTeam)
Utils.loadImage(team.shield, imgTeam, context, R.drawable.team_placeholder)

configureTransition(team.id)
} else {
bindDataStats(team)
}
}*/